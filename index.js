const express = require('express');
const app = express();
const PORT = process.env.PORT || '8000';

/**
 * @api {get} /{id} hello world sample request
 * @apiName GetSample
 * @apiParam (Url) {String} message the message to return
 * @apiSuccess {String} data the hello world data
 * @apiSuccess {String} output what the user entered in the url
 */
app.use((req, res, next) => {
  console.log({ method: req.method, url: req.originalUrl, date: new Date() });
  return next();
});
app.get('/:message', (req, res) => res.send(req.params.message));

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

